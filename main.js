
$(function() {

	function PageController() {
		var self = this;
		this.startButton = $('#start-button');

		this.recognition = new webkitSpeechRecognition();
		this.recognition.continuous = false;
		this.recognition.interimResults = true;
		this.recognition.lang = 'es-MX';

		this.utterance = new SpeechSynthesisUtterance();
		this.utterance.lang = 'fr-FR';

		this.finalTranscript = '\n';

		this.recognition.onresult = function(event) {

			var interimTranscript = '';
			
			for (var i = event.resultIndex; i < event.results.length; ++i) {

				var text = event.results[i][0].transcript;

				if (text !== 'undefined') {
					if (event.results[i].isFinal) {
		        		self.finalTranscript += text;
						self.utterance.text = self.finalTranscript;
		      		} else {
		        		interimTranscript += text;
		      		}
				}
			}
  		};

  		this.recognition.onend = function() {
  			self.pintaChubaca(self.finalTranscript);
  			self.finalTranscript = '';
  		};

	}


	PageController.prototype.initListeners = function () {
		var self = this;

		this.startButton.click(function(e) {
			self.recognition.start();
		});
	};

	PageController.prototype.pintaChubaca = function(string) {
		var aleatorio = Math.round(Math.random()*17);
		var nuevaSrc = "img/"+ aleatorio + ".gif";
		$("img.chuwi").attr("src", nuevaSrc);
		$("img.chuwi").addClass('activo');
		$("#mensaje").html(string);
		//window.speechSynthesis.speak(this.utterance);
		var randomSoundIndex = Math.round(Math.random() * 8);
		var audio = $('<audio autoplay><source src="sounds/'+ randomSoundIndex +'.mp3" type="audio/mpeg"></audio>');

		$('body').append(audio);
	};

	function main() {
		var page = new PageController();
		page.initListeners();
	}

	main();
});



